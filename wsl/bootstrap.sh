#!/bin/bash

if [ "$(umask)" = "0000" ]; then
    umask 022
fi

sudo add-apt-repository ppa:neovim-ppa/stable -y
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -

sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install -y \
    neovim \
    build-essential \
    nodejs \
    gnome-terminal \
    silversearcher-ag
sudo npm install -g diff-so-fancy

sudo update-alternatives --install /usr/bin/vi vi /usr/bin/nvim 60
sudo update-alternatives --install /usr/bin/vim vim /usr/bin/nvim 60
sudo update-alternatives --install /usr/bin/editor editor /usr/bin/nvim 60
