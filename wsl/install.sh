#!/bin/bash

BASEDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# create config folder
mkdir -p ~/.config

# top level configs
rm ~/.bashrc
ln -s ${BASEDIR}/bashrc ~/.bashrc
ln -s ${BASEDIR}/fonts.conf ~/.fonts.conf
ln -s ${BASEDIR}/gitconfig ~/.gitconfig
ln -s ${BASEDIR}/gitignore_global ~/.gitignore_global

# neovim
mkdir -p ~/.config/nvim
ln -s ${BASEDIR}/init.vim ~/.config/nvim/init.vim

# download vim-plug for neovim
if [ ! -f ~/.local/share/nvim/site/autoload/plug.vim ]; then
    curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
fi
