#!/bin/bash

BASEDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# top level configs
ln -s ${BASEDIR}/bashrc ~/.bashrc
ln -s ${BASEDIR}/gitconfig ~/.gitconfig
ln -s ${BASEDIR}/i3status.conf ~/.i3status.conf
ln -s ${BASEDIR}/vimrc ~/.vimrc
ln -s ${BASEDIR}/tmux.conf ~/.tmux.conf
ln -s ${BASEDIR}/livestreamerrc ~/.livestreamerrc

# i3 folder
mkdir -p ~/.i3
ln -s ${BASEDIR}/i3/config ~/.i3/config

# config folder
mkdir -p ~/.config/terminator
ln -s ${BASEDIR}/config/redshift.conf ~/.config/redshift.conf
ln -s ${BASEDIR}/config/terminator/config ~/.config/terminator/config

# stack folder
mkdir -p ~/.stack
ln -s ${BASEDIR}/stack/config.yaml ~/.stack/config.yaml

# Install Vundle
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
