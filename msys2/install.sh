#!/bin/bash

BASEDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# minttyrc
ln -s ${BASEDIR}/minttyrc ~/.minttyrc
