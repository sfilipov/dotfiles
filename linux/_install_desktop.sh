#!/bin/bash

# create config folder
mkdir -p ~/.config

# top level configs
ln -s ${BASEDIR}/i3status.conf ~/.i3status.conf
ln -s ${BASEDIR}/i3exit ~/.local/bin/i3exit
ln -s ${BASEDIR}/livestreamerrc ~/.livestreamerrc
ln -s ${BASEDIR}/xmobar_volume.sh ~/.xmobar_volume.sh
ln -s ${BASEDIR}/xmobarrc ~/.xmobarrc

# i3
mkdir -p ~/.i3
ln -s ${BASEDIR}/i3/config ~/.i3/config

# terminator and redshift
mkdir -p ~/.config/terminator
ln -s ${BASEDIR}/config/redshift.conf ~/.config/redshift.conf
ln -s ${BASEDIR}/config/terminator/config ~/.config/terminator/config

# xmonad
mkdir -p ~/.xmonad
ln -s ${BASEDIR}/xmonad/xmonad.hs ~/.xmonad/xmonad.hs
