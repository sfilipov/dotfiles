#!/bin/bash

# create config folder
mkdir -p ~/.config

# top level configs
ln -fs ${BASEDIR}/bashrc ~/.bashrc
ln -fs ${BASEDIR}/gitconfig ~/.gitconfig
ln -fs ${BASEDIR}/gitignore_global ~/.gitignore_global
ln -fs ${BASEDIR}/tmux.conf ~/.tmux.conf
ln -fs ${BASEDIR}/ssh_rc ~/.ssh/rc

# install basic tools
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install -y \
    build-essential \
    python-dev \
    python3-dev \
    silversearcher-ag \
    tree \
    curl
