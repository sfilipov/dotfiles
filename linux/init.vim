call plug#begin('~/.local/share/nvim/plugged')

Plug 'bronson/vim-trailing-whitespace'
" Plug 'christoomey/vim-tmux-navigator'
Plug 'flazz/vim-colorschemes'
Plug 'kien/ctrlp.vim'
Plug 'mileszs/ack.vim'
" Plug 'pbrisbin/vim-syntax-shakespeare'
" Plug 'rbgrouleff/bclose.vim'
Plug 'scrooloose/nerdcommenter'
Plug 'scrooloose/syntastic'
" Plug 'tpope/vim-fugitive'
" Plug 'tpope/vim-surround'

" haskell related
" Plug 'eagletmt/ghcmod-vim'
" Plug 'eagletmt/neco-ghc'
" Plug 'tomtom/tlib_vim'
" Plug 'MarcWeber/vim-addon-mw-utils'
" Plug 'garbas/vim-snipmate'
" Plug 'godlygeek/tabular'
" Plug 'ervandew/supertab'
" Plug 'Shougo/neocomplete'
" Plug 'Shougo/vimproc.vim', {'do' : 'make'}

" purescript
Plug 'purescript-contrib/purescript-vim'
Plug 'FrigoEU/psc-ide-vim'


call plug#end()

" leave space on top and bottom
set so=15

" search case insensitive
set ignorecase
set smartcase

" switch buffers without saving
set hidden

" show line and column number in status bar
set ruler

colorscheme google

let g:ctrlp_show_hidden = 1
let g:ctrlp_user_command = ['.git/', 'git --git-dir=%s/.git ls-files -oc --exclude-standard']

" F5 - toggle paste
set pastetoggle=<F5>

" F6 - list buffers and switch
nnoremap <F6> :buffers<CR>:buffer<Space>

" space is leader key
let mapleader = "\<Space>"

" work with old versions of ag
if executable('ag')
  let g:ackprg = 'ag --vimgrep'
endif

" show tabs as 2, > moves 2, expand tabs to spaces
filetype plugin indent on
set tabstop=2
set shiftwidth=2
set expandtab


" syntastic
" map <Leader>s :SyntasticToggleMode<CR>

" set statusline+=%#warningmsg#
" set statusline+=%{SyntasticStatuslineFlag()}
" set statusline+=%*

" let g:syntastic_always_populate_loc_list = 1
" let g:syntastic_auto_loc_list = 0
" let g:syntastic_check_on_open = 0
" let g:syntastic_check_on_wq = 0

" get ghc-mod type
" nnoremap <silent>,ht :GhcModType<CR>

" clear highlighting from search and ghc-mod
" nnoremap <silent><Esc> :nohlsearch <bar> :GhcModTypeClear<CR><Esc>
