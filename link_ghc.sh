#!/bin/bash

GHC_VERSION="7.10.2"
GHC_PATH="${HOME}/.stack/programs/x86_64-linux/ghc-${GHC_VERSION}"

ln -s ${GHC_PATH}/bin/ghc ${HOME}/.local/bin/ghc
ln -s ${GHC_PATH}/bin/ghci ${HOME}/.local/bin/ghci
ln -s ${GHC_PATH}/bin/ghc-pkg ${HOME}/.local/bin/ghc-pkg
