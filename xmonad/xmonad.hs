import XMonad
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig(additionalKeys)
import Graphics.X11.ExtraTypes.XF86
import System.IO

main = do
    xmproc <- spawnPipe "stalonetray"
    xmproc <- spawnPipe "compton -b"
    xmproc <- spawnPipe "nm-applet --sm-disable &"
    xmproc <- spawnPipe "dropbox"
    xmproc <- spawnPipe "xmobar"


    xmonad $ defaultConfig
        { terminal    = "urxvt"
        , borderWidth = 2
        , manageHook = manageDocks <+> manageHook defaultConfig
        , layoutHook = avoidStruts  $  layoutHook defaultConfig
        , logHook = dynamicLogWithPP xmobarPP
                        { ppOutput = hPutStrLn xmproc
                        , ppTitle = xmobarColor "green" "" . shorten 50
                        }
        , modMask = mod4Mask     -- Rebind Mod to the Windows key
        } `additionalKeys`
        [ ((mod4Mask .|. shiftMask, xK_l), spawn "slock")
        , ((controlMask, xK_Print), spawn "sleep 0.2; scrot -s")
        , ((0, xK_Print), spawn "scrot")
        , ((0, xF86XK_AudioLowerVolume), spawn "amixer set Master 5%-")
        , ((0, xF86XK_AudioRaiseVolume), spawn "amixer set Master 5%+")
        , ((0, xF86XK_AudioMute), spawn "amixer set Master toggle")
        ]
